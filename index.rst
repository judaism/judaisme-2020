.. index::
   ! Judaïsme 2020


.. raw:: html

   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


|FluxWeb| `RSS <https://judaism.gitlab.io/judaisme-2020/rss.xml>`_

.. _judaisme_2020:

=======================================================================================================
🌱 **Judaïsme 2020** 🕊🌎🕯
=======================================================================================================

- https://fr.wikipedia.org/wiki/Portail:Juda%C3%AFsme
- https://www.jewfaq.org/index.shtml


.. toctree::
   :maxdepth: 4

   10/10
   08/08
