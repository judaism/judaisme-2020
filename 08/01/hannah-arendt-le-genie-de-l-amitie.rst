

.. _birnbauim_2020_08_01:

=======================================================================================================
2020-08-01 **Hannah Arendt, le génie de l’amitié** par Jean Birnbaum
=======================================================================================================

- https://www.lemonde.fr/series-d-ete/article/2020/08/24/hannah-arendt-le-genie-de-l-amitie_6049816_3451060.html
- https://www.editionsdelherne.com/publication/cahier-hannah-arendt/


Le courage de la nuance" (2/6).
======================================

Contre la pensée dogmatique, certaines figures du XXe siècle ont incarné
l’audace de l’incertitude.

Pour la philosophe, la bêtise se combat par le dialogue sincère, où peut se
déployer "l’antique vertu de modération"

Lors d’une visite sur le front de la Grande Guerre, l’empereur allemand,
Guillaume II, aurait fait cette déclaration, au milieu des cadavres :
"Je n’ai pas voulu cela."

Trente ans plus tard, en 1945, commentant ces mots avec, à l’esprit, un autre
carnage mondial, la philosophe Hannah Arendt écrivait :
"Ce qu’il y a d’effroyablement comique, c’est que c’est vrai."

Encore vingt ans plus tard, à l’occasion d’un entretien à la télévision, elle
trouvera le même effet comique chez d’autres "responsables irresponsables"
qui ont provoqué des massacres, à commencer par le criminel de guerre nazi
Adolf Eichmann : "J’ai lu son interrogatoire de police, soit 3 600 pages,
de très près, et je ne saurais dire combien de fois j’ai ri, ri aux éclats !",
confiait-elle.

**Ce rire-là est resté en travers de quelques gorges. Il ne fut pas pour rien
dans la vaste controverse qui suivit la parution d’Eichmann à Jérusalem
(1963 ; Gallimard, 1966)**, réflexion en forme de reportage pour le magazine
américain The New Yorker, qui avait envoyé Arendt suivre le procès.

Rédigé d’une plume sardonique, ce texte dépeint Eichmann non pas comme un monstre
sanguinaire, mais comme un clown grotesque.
Tout en regrettant que ce ton narquois ait pu blesser certains lecteurs,
Arendt n’en revendiquera pas moins la nécessité.
"Je continue à penser qu’on doit pouvoir rire, parce que c’est en cela que
consiste la souveraineté, et que toutes ces objections contre l’ironie me
sont d’une certaine manière très désagréable, au sens du goût, c’est un fait",
précisera-t-elle à la radio.

Une langue vitrifiée
======================

- https://www.lemonde.fr/archives/article/2002/06/28/l-urgence-de-hannah-arendt_4230526_1819218.html

Afin d’expliquer ce goût tenace, on peut mentionner une sentence du
dramaturge allemand Bertolt Brecht, qu’Arendt aimait citer :
**"Il faut écraser les grands criminels politiques : et les écraser sous le
ridicule."**

Mais, aux yeux de la philosophe, ce parti pris constitue bien plus qu’une arme
politique. Il engage tout un rapport à la liberté de juger.

L’ironie introduit du jeu là où la pensée étouffe, elle remet le langage en
mouvement.
Or, pour l’autrice des `Origines du totalitarisme (1951 ; Gallimard, 2002) <https://www.lemonde.fr/archives/article/2002/06/28/l-urgence-de-hannah-arendt_4230526_1819218.html>`_,
l’expérience totalitaire est d’abord celle d’une langue vitrifiée.

La "banalité du mal", cette formule d’Arendt qui a provoqué tant de polémiques,
concerne moins les individus que leur discours.
L’homme du mal ne dit que des banalités ; Eichmann est "incapable de prononcer
une seule phrase qui ne fût pas un cliché".
Peu importe qu’il soit ou non un pauvre type, l’essentiel est qu’il débite
de misérables stéréotypes.

A ceux qui l’accusent d’avoir relativisé l’horreur en affirmant que chacun de
nous cache un génocidaire, Arendt répond ceci : "Eichmann n’était pas stupide.
**C’est la pure absence de pensée** – ce qui n’est pas du tout la même chose – qui
lui a permis de devenir un des plus grands criminels de son époque.

Cela est “banal” et même comique : avec la meilleure volonté du monde, on ne
parvient pas à découvrir en Eichmann la moindre profondeur diabolique ou
démoniaque.
Mais cela ne revient pas à en faire un phénomène ordinaire. Il n’est pas donné
à tout le monde de ne pouvoir évoquer, en montant sur l’échafaud, que les
phrases toutes faites que l’on prononce à tous les enterrements."

On l’aura compris, Arendt ne confond pas l’intelligence avec l’érudition,
ni l’audace avec la culture – elle connaît assez d’intellectuels pour
savoir que beaucoup d’entre eux, y compris parmi les plus prestigieux,
sont aussi médiocres que dociles. "Je pouvais constater que suivre le
mouvement était pour ainsi dire la règle parmi les intellectuels, alors
que ce n’était pas le cas dans les autres milieux. Et je n’ai jamais pu
oublier cela", se souvient cette juive allemande qui avait dû fuir son pays
après la prise du pouvoir par Hitler.

Pour elle, la "bêtise" désigne plutôt un certain rapport à soi, une
manière de coller à ses propres préjugés, jusqu’à devenir sourd aux vues
d’autrui.

**Vous vous adressez à quelqu’un et vous avez l’impression de parler à un mur ?**

**A coup sûr, vous touchez du doigt la bêtise**. Celle qui permet à un homme de
faire fonctionner une immense machine de mort sans éprouver le moindre scrupule,
parce que son entendement tourne à vide, et que ce pur fonctionnement le comble.

La pensée, un héroïsme ordinaire
=====================================

En cela, Arendt s’enracine profondément dans la philosophie antique,
et d’abord dans l’héritage socratique.

Si son ironie déstabilise ses interlocuteurs, ce n’est pas pour les paralyser,
mais au contraire pour les obliger à s’arrêter une seconde, à faire un retour
sur eux-mêmes, à renouer avec la liberté.
Pas de pensée sans dialogue, avec les autres et, pour commencer, avec soi.

"Parler avec soi-même, c’est déjà, au fond, la pensée", souligne-t-elle.
Avoir une conscience aux aguets, se montrer capable d’entrer dans une dissidence
intérieure, voilà le contraire du mal dans sa banalité : pour Arendt, la
pensée est un héroïsme ordinaire.

D’où son insistance sur le "manque d’imagination" d’Eichmann, **l’impossibilité
qui était la sienne de se mettre à la place des autres**.

C’est aussi la raison pour laquelle cet héroïsme de la pensée se confond
largement, chez elle, avec le génie de l’amitié : "C’est seulement
parce que je peux parler avec les autres que je peux également parler avec
moi-même, c’est-à-dire penser."
Le parcours intellectuel de la philosophe est structuré par cet idéal de l’amitié,
à la fois explication avec l’autre et élucidation de soi.

Toute jeune déjà, la petite Hannah, qui est née à Hanovre, en 1906, de
parents socialistes et cultivés, se montre sans cesse avide de rencontres.
A 6 mois, "elle est en très bons termes avec n’importe qui, à quelques
exceptions près, et adore l’effervescence", note sa mère, Martha, dans
le journal quotidien qu’elle tient sous le titre Unser Kind ("Notre enfant"),
publié dans A travers le mur. Un conte et trois paraboles, d’Hannah
Arendt (Payot, 2017).

A 3 ans, tout en manifestant le désir de se forger une langue à soi, Hannah
apparaît "extrêmement vive, toujours impétueuse ; et très chaleureuse avec
les étrangers".

Lire aussi, sur "L’Humaine Condition" (2012) :Hannah Arendt, philosophe
d’action

Plus tard, elle gardera ces traits de caractère : parfois cassante et même
vacharde, elle ancrera son impatience dans une générosité exigeante.
En 1914, alors que son père vient d’être emporté par la maladie, la fille de
7 ans console sa mère à la manière d’une vieille camarade : "Tu sais,
maman, cela arrive à beaucoup de femmes." Dès lors, elle qui n’aura
jamais d’enfant s’adressera toujours en amie à ses proches, parents,
maris, amants ou même présumés ennemis.

Une prudence tout sauf théorique
==================================

En 1933, après avoir été arrêtée à Berlin pour "propagande mensongère", Arendt
tombe sur un policier apparemment mal à l’aise avec le nouveau pouvoir nazi,
et qui lui inspire d’emblée confiance.
De fait, il lui permettra de sortir et donc de quitter le pays : "Cet homme
qui m’avait arrêtée avait un visage si ouvert, si honnête", se souviendra Arendt.

Au même moment, elle verra se fermer bien d’autres faces, et cette déception
orientera désormais son rapport au monde : "Vous savez ce que c’est qu’une mise
au pas.

**Cela signifiait que les amis aussi s’alignaient !**

Le problème, le problème personnel n’était donc pas tant ce que faisaient nos
ennemis que ce que faisaient nos amis", se souvenait cette réfugiée qui avait gagné
la France, où elle fut internée au camp de Gurs, dans les Pyrénées, avant
de s’évader et de s’exiler aux Etats-Unis, en 1941.

Dès lors, à travers les années de précarité matérielle, morale et juridique
(elle reste apatride pendant dix ans), Arendt va envisager l’amitié comme
l’unique espace où peut se déployer "l’antique vertu de modération",
le seul lieu qui fait droit à cette pluralité où elle discerne le cœur
de notre condition humaine : "Exercer une influence, moi ? Non, ce que je
veux, c’est comprendre, et lorsque d’autres gens comprennent eux aussi,
je ressens alors une satisfaction comparable au sentiment que l’on éprouve
lorsqu’on se retrouve en terrain familier", dit-elle.

Arendt refuse de se dire philosophe, préférant se présenter comme "écrivain
politique". Et de même que sa prudence est tout sauf théorique, puisqu’elle
s’enracine dans les années de fuite et la "patience active" des parias,
de même son effort d’imagination se porte moins sur les idées que sur
les personnes, leur honneur vulnérable, leur dignité possible.

En ce sens, quiconque voudrait découvrir l’œuvre d’Arendt devrait commencer non
par tel ou tel volume théorique, mais par la correspondance si sensible,
si complice, avec Heinrich Blücher, son second mari, ou par le recueil
d’hommages émus intitulé Men in Dark Times ("Des hommes dans les
temps sombres") et traduit en français sous le titre Vies politiques
(Gallimard, 1974). On y trouve notamment des textes magnifiques consacrés
à la "politesse du cœur" qui distinguait le romancier Hermann Broch, à
la bouleversante solitude du critique Walter Benjamin, qui s’est donné la
mort, en 1940, à Port-Bou (Pyrénées), pour échapper à l’arrestation,
ou encore au politologue Waldemar Gurian, à propos duquel Arendt écrit :
"Il avait accompli ce qui est notre tâche à tous : établir sa demeure en
ce monde et la bâtir sur la terre grâce à l’amitié."

Même quand elle semblera péremptoire, comme elle le fut parfois dans
sa manière de prendre position contre la guerre du Vietnam ou pour les
droits civiques aux Etats-Unis, Arendt restera fidèle à cette conception de
l’amitié comme désir de la confrontation sincère.

Opposer l’idéologie à l’idéologie et les slogans aux slogans, lui apparaîtra
toujours comme un signe de lâcheté. A ses yeux, les certitudes des "sectes"
intellectuelles seront bien moins éclairantes que la "lumière incertaine,
vacillante et souvent faible" des êtres chers.

Lire ses textes, en hériter aujourd’hui, c’est relancer un art de la nuance
qui se confond avec la revendication de l’ironie et l’exigence de l’amitié.
